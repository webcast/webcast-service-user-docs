# FAQs

## How can I access a CERN webcast?

CERN webcasts can be mainly watched on https://webcast.web.cern.ch. Usually they are open to the public, i.e. world readable.

If you are Signed In and you click on a restricted webcast, it will start to play immediately provided you are in any of the groups allowed to watch the stream.

If you are not in this e-group, you will receive an error message.

If you think you should have access to a webcast, but you are unable to watch it due to permissions, [please open a ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&fe=webcast&s=webcast).

## Why do we need to request the webcast with 2 weeks in advance?

Organizing a webcast requires organization in both your side and our side, and we need to coordinate our resources in order to provide the best service possible. Therefore, we ask the organizers to do this with enough time in advance.

## Will there be someone in the room during the event?

The operator will arrive 15 minutes in advance and he/she will leave once the event has started.

Please, take this into account when you organize an event that requires special health meassures so he/she can access the room.

## Is it possible to have captions during the event?

-

## I need a webcast in several languages, what should I do?

-

## Can a webcast be restricted?

Yes, a webcast can be restricted to groups.

## Where are the recordings of my event published?

The recorded web lecture will be published in [CDS](https://cds.cern.ch). The video will be stored with the same access restrictions as the Indico event in CDS and directly accessible from the corresponding Indico page.

Please note that the same restrictions as "Organising a live webcast of an event in an equipped room" apply.

## How can I interact with my audience during a webcast?

Please read about possibilities at [LiveQnA](./liveqna.md).

## I see a spinning wheel and the stream doesn't start, what can I do?

Sometimes, a user can experience problems with the streams not loading properly and displaying an infinite spinning wheel. This often happens due to the player having syncronization problems on a camera + slides webcast, but it can happen due to other reasons.

### Solution 1: Reload the page and wait a few seconds

Sometimes, reloading the page and waiting a few seconds solves the issue.

### Solution 2: Select the camera/slides only view

![Dual or single view selector](./assets/images/dual-single-stream-view.png)

In case that the issue appears due to a syncronization problem between the camera and slides streams, you can switch to the single stream view mode, clicking on the "Camera" or "Slides" buttons as showed on the screenshot above.
### Solution 3: Add our streaming servers to your antivirus allow list

This applies to workstations that have installed an extra antivirus or malware software e.g. Avast. In general for CERN managed desktops this shouldnt be a concern.

Another cause that we saw several times is an Antivirus blocking the requests to our streaming servers.

The naming of this feature is different depending on the antivirus solution you are using: Web access protection, URL whitelist, etc. Please check your antivirus documentation in order to learn how to configure this settings.

The URL that must be added is the following:

- https://wowza.cern.ch

Once done, you may need to reload the page.

For machines outside CERN intranet please consider also checking if adding above URL at _Trusted Sites_ helps. To do so please navigate to Windows icon located located in the bottom-left corner of your screen, search for _Internet Options_, _Security_ tab and add the URL to _Trusted sites_.

### Solution 4: Avoid SSH tunneling while watching a webcast

Avoid the use of SSH tunneling while watching a webcast, (i.e. sshuttle).

## Which CERN rooms are equipped for Webcast and/or Recording ?

The full list of rooms available for Webcast and/or Recording is available on the [Indico Room Booking](https://indico.cern.ch/rooms/rooms?feat=webcast-recording) website.

## NS_ERROR_DOM_MEDIA_FATAL_ERR (0x8060005) - mozilla:MediaResult mozilla::FmpegDataDecoder<59>:InitDecoder): Couldn't open

![Error missing codecs on Alma9](./assets/images/ffmpeg-error-alma9.png)

If you experience this error when you try to access a webcast, you might need to install `ffmpeg` and `ffmpeg-libs` in your system.

This error was spotted on Alma9 and Firefox, but it might affect other Linux distributions and browsers.