# Useful Links

- [Webcast and Recording Service in CERN Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=webcast)
- [List of rooms capable of webcast and recording (Indico)](https://indico.cern.ch/rooms/rooms?feat=webcast-recording)
- [CERN Webcast Website](https://webcast.web.cern.ch/)
