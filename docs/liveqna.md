# LiveQnA

The Webcast technology is traditionally unidirectional. Attendees of an event display a webcast stream using player technology supported by a browser or integrated within the browser capabilities. 

In order to provide user interaction, one posibility is to use a LiveQnA site, where attendees or organisers could interact with the audience. 

This article examines the possibilities at CERN.

## Zoom Webcasts using Zoom Webinars

A Zoom Webinar can be "webcasted" using CERN Webcast service, please open a [SNOW ticket](https://cern.service-now.com/service-portal?id=service_element&name=webcast) or use Indico to submit the request to the service. 

For **attendees** joining Zoom Webinar, the **host** can setup Q&A for those: 

- Allow anonymous questions: Check this option to allow participants to send questions without providing their name to the host, co-host, and panelists.
- Allow attendees to view: Check either if you want attendees to be able to view answered questions only or view all questions.

If you choose for attendees to view all questions, you can then enable the following options: 

- Attendees can upvote: Attendees can view all submitted questions and upvote questions important to them. This can help point out to the host and co-hosts questions that more attendees want the answer to. 
- Attendees can comment: Attendees can view all submitted questions and add additional comments. 

The **host** and **co-host** then they can manage those question e.g. dimiss one, marked as replied, etc.

For a thorough overview please consult Zoom documentation: [Using Q and A as the webinar host](https://support.zoom.us/hc/en-us/articles/203686015-Using-Q-and-A-as-the-webinar-host).

!!! info
	In case you have more audience than the one allowed on Zoom webinar (at this date 1000 attendees is the maximum of CERN Zoom webinar license), we do recommend to use one of the next options and avoid LiveQnA conundrum. 

## LiveQnA on Mattermost

This is a solution provided by our Mattermost colleague, Adrian Moennich.
It's an ad hoc solution, which has been used in Townhall events.

It's made of two interfaces: for attendees and moderators.

- For _attendees_: an URL (an app running on OKD) that attendees will use to post, **annonimously or not**, questions. The URL is customized per event. The attendees interface is under **CERN SSO**. Access can also be restricted to a particular egroup/grappa group.

![](./assets/images/liveqa01.png)

- For _moderators_: You can dismiss a question, mark it as replied and set it under a category that you can also define on your SNOW ticket. In order to do your job, you will use [Mattermost](https://mattermost.web.cern.ch). Two channels will be created e.g.  _Q&A Questions_ and _Q&A Accepted_ ones. You can also group questions using those categories. Please see next two pictures to get a feeling from the app:

![](./assets/images/liveqa02.png)

![](./assets/images/liveqa03.png)


**To notice** that with this solution, **audience cant see** questions submitted by themselves or other people. They **cant upvote** questions. It's the job of the moderator to filter/group questions.

!!! info
	You should indicate on your SNOW ticket that you would like to have this LiveQnA solution.

!!! info
	A request to Mattermost admin can be done to get an Excel with all questions.

## LiveQnA public

This is a solution presented by Alex Iribarren at [IT Lightning Talks: session #21](https://indico.cern.ch/event/1097664/). It has been used by IT management, Staff Association and general purpose events. 

The tool requires CERN SSO to access. **Questions and votes are annonymous**. 

It can be accessed at [https://liveqna.web.cern.ch/](https://liveqna.web.cern.ch/). 

By default all CERN users (primary accounts) will be able to access an event, though **egroups/grappa groups** can be used to limit audience. At creation and later those access groups plus the group for administration can be modified

![](./assets/images/liveqa04.png)

Once the event is created, people can use the QR or link to join it:

![](./assets/images/liveqa05.png)

While submitting a question, a user can decide which topic to attach to it. A moderator can also modified it afterwards.

Users can (up/down)vote just once per question. For the ranking algorithm in use please check: [Question Ranking Algorithm](https://gitlab.cern.ch/airibarr/liveqna/-/wikis/Question-Ranking-Algorithm).

![](./assets/images/liveqa06.png)

A user can just submit one question per minute, to avoid people abusing the system.
Privacy notice of the service can be consulted: [PN01163](https://cern.service-now.com/service-portal?id=privacy_policy&se=it-lightweight-services&notice=liveqna).

!!! tip
	The **main differences** with Mattermost related tool are: ease of event management, automatic ranking by the community votes and therefore questions are  public.