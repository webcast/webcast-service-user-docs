# Collect browser's console logs and network usage

Some issues on the client side, specific to a user can be difficult to troubleshoot if we don't have the detail information of what's going on the user's browser. The console logs allows us to debug issues that happen on your browser.

## Chrome or Edge

### Chrome Console logs

1. Click on the 3 dots menu in the Chrome's navigation menu.
2. Go to "More tools" and then "Developer tools.
3. Click on the "Console tab" in the Developer Tools panel:

![Chrome console tab](./assets/images/collect-logs/chrome-console-tab.png)

4. Ensure that at least, the "Verbose", "Info" and Errors" log levels are selected.

![Chrome log levels](./assets/images/collect-logs/chrome-log-levels.png)

5. Right click on the log area and select "Save as..."

![Chrome log levels](./assets/images/collect-logs/chrome-save-logs-as.png)

6. Finally open the file, copy it's content and add it to the ticket or share the file on the ticket.

### Chrome Network usage

1. Click on the 3 dots menu in the Chrome's navigation menu.
2. Go to "More tools" and then "Developer tools.
3. Click on the "Network tab" in the Developer Tools panel
4. Reload de page (Ctrl + R / Cmd + R)
5. Click on the play button and keep it running at least 2 minutes.
6. Then, click on the content of the "name" column and then select "Save all as HAR with content"

![Chrome Network logs](./assets/images/collect-logs/chrome-network-logs.png)

## Firefox

### Firefox console logs

1. Click on the 3 bars menu in the Firefox's navigation menu.
2. Go to "More tools" and then "Web Developer Tools.
3. Click on the "Console tab" in the Developer Tools panel:

![Firefox console tab](./assets/images/collect-logs/firefox-console-tab.png)

4. Right click on the log area and select "Save all message to File"

![Firefox collect logs](./assets/images/collect-logs/firefox-save-logs.png)

5. Finally open the file, copy it's content and add it to the ticket or share the file on the ticket.

### Firefox Network usage

1. Click on the 3 dots menu in the Firefox's navigation menu.
2. Go to "More tools" and then "Web Developer Tools.
3. Click on the "Network tab" in the Developer Tools panel
4. Reload de page (Ctrl + R / Cmd + R)
5. Click on the play button and keep it running at least 2 minutes.
6. Then, click on the content of the "name" column and then select "Save all as HAR"

![Firefox Network logs](./assets/images/collect-logs/firefox-network-logs.png)


## Safari

### Safari console logs

1. Click on the "Safari" menu item and then "Settings...".
2. Go to the "Advanced" tab and check "Show develop menu in menu bar". Close settings.
3. In the menu bar, select "Develop" and then "Show Javascript console".

![Safari console tab](./assets/images/collect-logs/safari-console-tab.png)

1. Press "cmd + a" to select all the logs. Then, right click on the logs area and select "Save selected".

![Safari collect logs](./assets/images/collect-logs/safari-save-logs.png)

5. Finally open the file, copy it's content and add it to the ticket or share the file on the ticket.

### Safari Network logs

1. Click on the "Safari" menu item and then "Settings...".
2. Go to the "Advanced" tab and check "Show develop menu in menu bar". Close settings.
3. In the menu bar, select "Develop" and then "Show Javascript console".
4. Go to the "Network" tab.
5. Reload de page (Ctrl + R / Cmd + R)
5. Click on the play button and keep it running at least 2 minutes.
6. Then, click on the "Export" button on the top right corner.

![Safari network logs](./assets/images/collect-logs/safari-network-logs.png)