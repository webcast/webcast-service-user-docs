# Working hours

This document includes information about the working hours of the Webcast Service.

- **Normal working hours**: 8:30 - 17:30
- **Out-of-hours support**: [Please open a ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&fe=webcast&s=webcast)

All the cases during Out-of-hours (including real time troubleshooting) will be taken in a best effort basis (no guaranteed service).