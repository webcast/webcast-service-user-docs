# Webcast and Recording Service User Documentation

Welcome to the CERN's webcast service user documentation.

The CERN Webcast Service opens CERN lectures, meetings and seminars to universities, schools and to the general public.

This service allows you to watch events from your desktop computer, 'as they happen'. It also allows people to watch live events from equipped rooms on https://webcast.web.cern.ch and provides video recording of these events for future viewing.

It is also responsible for providing the video streaming capabilities when watching past events recorded in video from CDS (https://cds.cern.ch) or Indico (https://indico.cern.ch). The service is also responsible for the audio/video retransmission of events between the main CERN rooms.

## What can we provide?

- Our infrastructure is capable of handling streams using `RTMP` and `RTSP` protocols.
- Display the webcast on the CERN's webcast website (https://webcast.web.cern.ch)
- A recording of the event.

## Brief description of the service

Please see below the architecture of the service:

![](./assets/images/webcast01.png)

A deeper description can be read at this presentation about [how  VIP event is conceived](./assets/documents/rgaspar_dgtalk.pdf).