# Check stats from a webcast

You can watch the stats of a given event on the [Webcast service stas page](https://conferencing-stats.app.cern.ch/d/E402gsj4z/webcast?orgId=1), which is available from inside CERN network. 

Please notice that depending on the **interval used by the Graphana panel**, the representation could be more or less spiky, please see the graph for the same event where the interval has been progressively increased to 20 minutes:

![](./assets/images/stats01.png)

![](./assets/images/stats02.png)

![](./assets/images/stats03.png)


Adjust the graph timespan so you get your event within it. The event is identified by your Indico event id. **Please do a printscreen for archival purposes**, data is only kept for 1 year. 