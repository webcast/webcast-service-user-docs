# Organising a live webcast of an event in an equipped room

This document describes the steps to follow to organize a Live event at CERN and includes some technical information that can be useful for organizers.

Please take a few minutes to read carefully the following documentation.

!!! info

    **Recording authorization**: Please note that it is your responsibility as organiser of the event to request from the speaker his/her **authorisation for being recorded**. Indico provides a handy interface to collect these authorisations in an electronic way.

## Communications between the organizers and the webcast team

It is very important that, in order to improve the organisation of the event, all communications between the organisers and the webcast team (us) [must be done using a ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=webcast). This is important in order to avoid misunderstandings and loss of information that usually occur in email threads.

If you wish to provide files (subtitles, documents, etc.), you should also do so via a ticket to avoid losing them.

## 1. Prerequisites

!!! warning

    Please, be aware of the warning messages displayed on Indico. Webcast and/or recording will be rejected if any of the following does not comply.

As an organizer, **at least 2 weeks before the event or 1 month if the event takes place on a weekend or a [official holiday](https://home.cern/official-holidays))** , you must:

1. **Check if the room is equipped** with the webcast and recording equipement at : [CERN webcast/recording rooms](https://indico.cern.ch/rooms/rooms?feat=webcast-recording).

2. Event types:
      - Conference: Event must be a conference and include at least one contribution.
      - Weblecture: Event must be a weblecture with no contributions.
      - Meeting: __Webcast and/or recordings are not possible__

3. **Create an event in Indico** and in the management area for this event, go to the "Services -> Logistics" menu item, and click on the "Webcast/Recording"  and fill the form.
 - Please, be as specific as you can:
     1. Will the event have several languages?
     2. Will it use captions?
     3. Stream to social Media?
     4. How many people do you expect to attend the webcast?
     5. Is there something that makes it special?
4. Once your request is accepted, you will receive a confirmation email.

!!! warning

    Support outside working hours will be charged, so please provide a budget code with your request.

## 2. Rehearsal sessions

In case of a high profile webcast, **we recommend to run, at least, one rehearsal session a few days before** so we can test the setup and we can identify possible issues. Some common issues are the microphone volume being too low, unstable bandwidth, etc.

## 3. Before the webcast starts

We will need to start streaming with some time in advance to troubleshot any possible problems as soon as possible before the webcast starts.

- **Standard webcast**: 10 minutes in advance
- **Special or high profile webcast**: We recommend to start the streaming earlier (20-30 minutes in advance).

## 4. During the webcast

During the event you will need to make sure that speakers are equipped with an open microphone, unless you have requested the presence of a technician.

Our operation team will start, operate and stop the webcast remotely. In case of any problem, before, during or after the webcast please contact the support team by calling the **helpdesk (77777)** or [opening a support ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&se=webcast).

## 5. After the webcast

Please note that these features are independent from each other:

- **DVR**: When the webcast is unpublished on the webcast website, a recording of the event will remain available on the "Recent Webcasts" section for 1 week, thanks to the DVR feature of our webcast infrastructure. Please note that this time cannot be changed.

- **Publication of the recording**: The publication of the recording in the CERN Document Server (CDS), which may take several hours or days to be completed will also happen after the event finishes, and as soon as the speaker release form has been signed.
