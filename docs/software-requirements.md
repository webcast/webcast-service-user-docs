# Software requirements to watch a webcast

- **Windows/Mac**: You will need a recent version of Chrome, Firefox or Safari (Other browsers may work too).
- **Linux**: It may be necessary to install some libraries (ffmpeg). Tested on Firefox.
- **iOS/Android**: You will need a recent version of Chrome or Safari.

## Install FFMPEG on Centos 7,8 and 9

The following commands were provided by the CERN's Linux support.

```bash
sudo yum -y install epel-release
sudo yum -y install https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm
sudo yum -y install ffmpeg
```