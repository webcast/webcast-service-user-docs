FROM centos/python-38-centos7

EXPOSE 8000

WORKDIR /opt/app-root/src

ADD ./requirements.txt /opt/app-root/src/requirements.txt
RUN pip install -r requirements.txt

ENTRYPOINT ["mkdocs"]
CMD ["serve", "--dev-addr=0.0.0.0:8000", "--verbose"]