# Webcast Service user docs

Deployment uses Gitlab pages and Webeos. The application is deployed automatically using the `.gitlab-ci.yml` file.

Permissions can be managed through the Application's portal

## Links

- How to docs: https://how-to.docs.cern.ch/
- Webeos: https://webeos.cern.ch
- Applications portal: https://application-portal.web.cern.ch/

